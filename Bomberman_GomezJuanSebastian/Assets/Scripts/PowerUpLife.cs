﻿using UnityEngine;

public class PowerUpLife : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            ExtraLife();
            HudManager.OnBlockDestroy?.Invoke();
            Destroy(gameObject);
        }
    }
    private void ExtraLife()
    { 
        if (GameManager.Get().life == 3) return;
        GameManager.Get().life ++;
    }
}


