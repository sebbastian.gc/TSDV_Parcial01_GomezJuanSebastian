﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
public class HudManager : MonoBehaviour
{
    public Text textScore;
    public Text textClock;
    public Text textEnemieskilled;
    public Text textWin;
    public RawImage lifeOne;
    public RawImage lifeTwo;
    public RawImage lifeThree;
    public RawImage bomb;
    public GameObject screenGameOver;
    private TimeSpan timer;
    private bool timerBool;
    private float currentTime;
    
    
    public delegate void BlockDestroy();
    public static BlockDestroy OnBlockDestroy;

    private void Awake()
    {
        textClock.text = "00:00";
        textEnemieskilled.text = "0";
        timerBool = false;
        lifeThree.enabled = false;
        bomb.enabled = false;
    }
    private void Start()
    {
        textScore.text = "Score:" + GameManager.Get().score;
        Character.OnPlayerKill += CurrentLife;
        Enemy.OnDeath += CurrentScore;
        OnBlockDestroy += CurrentScore;
        OnBlockDestroy += CurrentLife;
        InitTimer();
        LevelManager.OnEndGame += GameOver;
    }
    private void OnDisable()
    {
        OnBlockDestroy -= CurrentScore;
        Character.OnPlayerKill -= CurrentLife;
        Enemy.OnDeath -= CurrentScore;
        LevelManager.OnEndGame -= GameOver;
        OnBlockDestroy -= CurrentLife;
    }
    public void ChangeScene(string scene)
    {
       GameManager.Get().Reset();
        SceneManager.LoadScene(scene);
    }

    private void CurrentScore()
    {
        textEnemieskilled.text = "" + GameManager.Get().enemiesKilled;
        textScore.text = "Score:" + GameManager.Get().score;
        if (!GameManager.Get().powerUpBomb) return;
        bomb.enabled = true;
    }

    private void CurrentLife()
    {   
        switch (GameManager.Get().life)
        {
            case 0:
                lifeOne.enabled = false;
                lifeTwo.enabled = false;
                lifeThree.enabled = false;
                break;
            case 1:
                lifeOne.enabled = true;
                lifeTwo.enabled = false;
                lifeThree.enabled = false;
                break;
            case 2:
                lifeOne.enabled = true;
                lifeTwo.enabled = true;
                lifeThree.enabled = false;
                break;
            case 3:
                lifeOne.enabled = true;
                lifeTwo.enabled = true;
                lifeThree.enabled = true;
                break;
        }
    }

    private void InitTimer()
    {
        timerBool = true;
        currentTime = 0;

        StartCoroutine("UpdateTime");
    }

    private void EndTime()
    {
        timerBool = false;
    }

    private IEnumerator UpdateTime()
    {
        while (timerBool)
        {
            currentTime += Time.deltaTime;
           timer =TimeSpan.FromSeconds(currentTime);
           string timerStr =" "+ timer.ToString("mm':'ss");
           textClock.text = timerStr;
           yield return null;
        }
    }

    private void GameOver()
    {
        if (!GameManager.Get().gameOver) return;
        EndTime();
        GameManager.Get().initGame = false;
        screenGameOver.SetActive(true);
        if (GameManager.Get().isWin)
            textWin.gameObject.SetActive(true);
    }
}
