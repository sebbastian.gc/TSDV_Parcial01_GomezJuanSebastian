﻿using System.Collections;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public bool initGame;
    public bool gameOver;
    public bool isWin;
    [HideInInspector]public bool powerUpBomb;
    [HideInInspector]public short activeBomb;
    [HideInInspector]public bool activeDoor;
    [HideInInspector]public short enemiesKilled;
    public short score;
    public short life;
    public delegate void Init();
    public static Init OnInitGame;
    private static GameManager instance;

    public static GameManager Get()
    {
        return instance;
    }

    private void Awake()
    {
        if (instance != null)
        {
            Destroy(gameObject);
            return;
        }

        instance = this;
        DontDestroyOnLoad(gameObject);
    }
    private void GameOver()
    {

    }
    public void InitGame()
    {
        StartCoroutine("TimeToInitGame");
    }

    private IEnumerator TimeToInitGame()
    {
        yield return new WaitForSeconds(1);
        initGame = true;
        OnInitGame?.Invoke();
    }
    public void Reset()
    {
        gameOver = false;       
        score = 0;
        life = 2;
        activeBomb = 0;
        activeDoor = false;
        enemiesKilled = 0;
        isWin = false;
        initGame = false;
    }

}
