﻿using UnityEngine;

public class Explosion : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    { string collider = other.gameObject.tag;

        switch (collider)
        {
            case "Player":
                Destroy(gameObject);
                Character.OnPlayerKill?.Invoke();
                break;
            case "Breakable":
                other.GetComponent<Block>().DestroyBlock();
                HudManager.OnBlockDestroy?.Invoke();
                Destroy(other.gameObject);
                break;
            case "Enemy":
                Enemy.OnDeath?.Invoke();
                Destroy(other.gameObject);
                break;
        }
       
    }
}
