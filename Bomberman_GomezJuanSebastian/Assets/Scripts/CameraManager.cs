﻿
using UnityEngine;

public class CameraManager : MonoBehaviour
{   public GameObject target;
    [Header("Camera Distance")][SerializeField] private Vector3 distance;
    [Header("Camera Rotation")] [SerializeField] private Vector3 angle;

    private void LateUpdate()
    {   transform.rotation=Quaternion.Euler(angle);
        transform.position = (target.transform.position +distance);
    }

}
