﻿using System;
using System.Collections;
using UnityEngine;
using Random = UnityEngine.Random;

public class Enemy : MonoBehaviour
{
    public Transform originRay;
    [SerializeField] private float speed;
    [SerializeField] private float rayDistance;
    [SerializeField] private LayerMask colliderTo;
    private enum Directions { Forward, Back, Left, Right };
    private Directions _currentDirection;
    private Vector3 _position;

    public delegate void Death();
    public static Death OnDeath;
    
    private void Awake()
    {
       RandDirection();
    }
    private void Start()
    {
        GameManager.Get().InitGame();
    }
    private void OnCollisionEnter(Collision other)
    {
        string collider = other.gameObject.tag;
        if (GameManager.Get().initGame) return;
        switch (collider)
        {
            case "Player":
            case "Breakable":
            case "Enemy":
                ReSpawn();
                break;
        }
    }

    private void ReSpawn()
    {
        _position.x = RandomPosition();
        _position.y = 0;
        _position.z = RandomPosition();
        transform.position = _position;
    }

    private float RandomPosition()
    {
        float num;
        do
        {
            num = Random.Range(0, 21);

        } while (num % 2 != 0);

        return num;
    }

    private void FixedUpdate()
    {
        if (!GameManager.Get().initGame) return;
        switch (_currentDirection)
        {
            case Directions.Forward:
                Restrictions(originRay.forward, 0);
                break;
            case Directions.Back:
                Restrictions(originRay.forward * -1, 180);
                break;
            case Directions.Left:
                Restrictions(originRay.right * -1, -90);
                break;
            case Directions.Right:
                Restrictions(originRay.right, 90);
                break;
        }
    }

    private void Move()
    {
        transform.position += transform.forward * (speed * Time.fixedDeltaTime);
    }

    private void Restrictions(Vector3 direction, float angle)
    {
        RaycastHit hit;
        if (Physics.Raycast(originRay.position, direction, out hit, rayDistance, colliderTo))
            RandDirection();
        else
        {
            transform.rotation = Quaternion.Euler(0, angle, 0);
            originRay.rotation = Quaternion.Euler(0, 0, 0);
            Move();
        }
    }

    private void RandDirection()
    {
        float rand = Random.Range(0, 4);
        switch (rand)
        {
            case 0:
                _currentDirection = Directions.Forward;
                break;
            case 1:
                _currentDirection = Directions.Back;
                break;
            case 2:
                _currentDirection = Directions.Right;
                break;
            case 3:
                _currentDirection = Directions.Left;
                break;
        }
    } 
    
}
    
