﻿using System;
using UnityEngine;
using Random = UnityEngine.Random;

public class LevelManager : MonoBehaviour
{
    [SerializeField, Range(1, 100)] private short quantityBlocks = 1;
    [SerializeField, Range(1, 20)] private short quantityEnemys = 1;
    private short CurrentQuantityBlocks;
    private short CurrentQuantityEnemies;
    public GameObject block;
    public GameObject enemy;
    public Transform containerBlocks;
    public Transform containerEnemyS;
    private Vector3 _position;
   // private 
    
    public delegate  void ReSpawn(Vector3 position);
    public static ReSpawn OnReSpawn;
    public delegate void EndGame();
    public static EndGame OnEndGame;

    private void Awake()
    {
        Spawn(enemy,quantityEnemys,containerEnemyS);
        OnEndGame += OpenDoor;
    }
    private void Start()
    {   
        Spawn(block,quantityBlocks,containerBlocks);
        CurrentQuantityBlocks = quantityBlocks;
        CurrentQuantityEnemies = quantityEnemys;
    }

    private void OnEnable()
    {
        OnReSpawn = Spawn;
        Block.OnBreak = DoorActive;
        Enemy.OnDeath += EnemiesKilled;
       
    }
    private void OnDisable()
    {
        OnReSpawn -= Spawn;
        Block.OnBreak -= DoorActive;
        Enemy.OnDeath -= EnemiesKilled;
        OnEndGame -= OpenDoor;
    }

    private void Spawn(GameObject prefab, short quantity,Transform container)
    {
        for (short i = 0; i < quantity; i++)
        {
            if (prefab.name=="Enemy")
            {
                _position.x = RandomPosition();
                _position.z = RandomPosition();
            }
            else
            {
                _position.x = Random.Range(0,21);
                _position.z = Random.Range(0,21); 
            }
            prefab.transform.position = _position;
            Instantiate(prefab,prefab.transform.position, prefab.transform.rotation,container);
        }
    }
    
    private float RandomPosition()
    {
        float num;
        do
        {
            num=Random.Range(0,21);
            
        } while (num % 2 != 0);
        return num;
    }
    
    private void Spawn(Vector3 position)
    { 
        position.x = Random.Range(0, 21);
        position.z = Random.Range(0, 21);
        transform.position = position;
    }

    private void DoorActive()
    {
        CurrentQuantityBlocks--;
        float num = Random.Range(0,21);

        if (CurrentQuantityBlocks == 1 && !GameManager.Get().activeDoor||num == 10 && !GameManager.Get().activeDoor)
             GameManager.Get().activeDoor = true;
    }

    private void EnemiesKilled()
    {
        GameManager.Get().score += 50;
        GameManager.Get().enemiesKilled++;
        CurrentQuantityEnemies--;
    }

    private void OpenDoor()
    {
        if (CurrentQuantityEnemies == 0 && GameManager.Get().activeDoor)
        {
            GameManager.Get().gameOver = true;
            GameManager.Get().isWin = true;
        }
    }

   
}

