﻿using System;
using System.Collections;
using UnityEngine;
public class Character : MonoBehaviour
{  
    public Transform originRay;
    public GameObject bomb;
    [SerializeField] private float speed;
    [SerializeField] private float rayDistance;
    [SerializeField] private LayerMask colliderTo;
    private Vector3 _initialPosition;
    public GameObject safeZone;
    private short totalBombs;
    public delegate void PlayerKill();
    public static PlayerKill OnPlayerKill;
  

    private void OnDisable()
    {
       transform.position = _initialPosition;
    }
    private void OnDestroy()
    {
        OnPlayerKill -= PlayerDeath;
    }

    private void Awake()
    {
        _initialPosition = transform.position;
        StartCoroutine("TimeToInit");
        totalBombs = 1;
        OnPlayerKill += PlayerDeath;
    }
    
    private IEnumerator TimeToInit()
    {
        yield return new WaitForSeconds(1);
        safeZone.SetActive(false);
    }

    private void Update()
    {
       Direction();
       CreateBomb();
    }

    private void Direction()
    {
        //visualizar rayos
        /*Debug.DrawRay(originRay.position, originRay.forward * (rayDistance), Color.red);
        Debug.DrawRay(originRay.position, originRay.right * rayDistance, Color.green);
        Debug.DrawRay(originRay.position, (originRay.forward * -rayDistance), Color.blue);
        Debug.DrawRay(originRay.position, (originRay.right * -rayDistance), Color.yellow);*/

        if (Input.GetKey(KeyCode.UpArrow))
            Restrictions(originRay.forward,0);
        
        else if (Input.GetKey(KeyCode.DownArrow))
            Restrictions(originRay.forward*-1,180);
        
        else if (Input.GetKey(KeyCode.LeftArrow))
            Restrictions(originRay.right*-1,-90);
        
        else if (Input.GetKey(KeyCode.RightArrow)) 
            Restrictions(originRay.right,90);
    }
    private void Restrictions(Vector3 direction,float angle)
    {
        RaycastHit hit;
        if (Physics.Raycast(originRay.position, direction, out hit, rayDistance, colliderTo))
            return;
        transform.rotation=Quaternion.Euler(0,angle,0);
        originRay.rotation=Quaternion.Euler(0,0,0);
        Move();
        
    }
    private void Move()
    {
        if (!GameManager.Get().initGame) return;
        transform.position += transform.forward * (speed * Time.deltaTime);
    }
    private void CreateBomb()
    {
        PowerUpBomb();
        if (GameManager.Get().activeBomb < totalBombs && Input.GetKeyDown(KeyCode.Space))
        {
            GameManager.Get().activeBomb++;
            Instantiate(bomb,CenterBomb(),Quaternion.identity);
        }
       
    }

    private void PowerUpBomb()
    {
        if (!GameManager.Get().powerUpBomb) return;
        totalBombs = 2;
    }
    private Vector3 CenterBomb()
    {
        Vector3 position;
        Vector3 currentPosition = transform.position;
        position.x = Mathf.Round(currentPosition.x);
        position.y = currentPosition.y - 0.58f; // deja la bomba a la altura del suelo 
        position.z = Mathf.Round(currentPosition.z);
        return position;
    }

    private void OnCollisionEnter(Collision other)
    {
        if (other.gameObject.CompareTag("Enemy"))
            OnPlayerKill?.Invoke();
    }
    private void PlayerDeath()
    {   GameManager.Get().life--;
        if ( GameManager.Get().life == 0)
        {
            GameManager.Get().gameOver = true;
            LevelManager.OnEndGame?.Invoke();
        }
        gameObject.SetActive(false);
        GameManager.Get().initGame = false;
        gameObject.SetActive(true);
        safeZone.SetActive(true);
        StartCoroutine("TimeToInit");
        GameManager.Get().InitGame();
    }

}
