﻿using UnityEngine;

public class Bomb : MonoBehaviour
{
    public ParticleSystem explosion;
    private BoxCollider _boxCollider;
    private SphereCollider _sphereCollider;
    [SerializeField] private float timeToExplosion;
    [SerializeField] private float rayDistance;
    private float _time ;
    
    private void Awake()
    {
       _boxCollider= GetComponent<BoxCollider>();
       _sphereCollider = GetComponent<SphereCollider>();
    }

    private void Update()
    {
       TimeToExplosion(timeToExplosion);
       
    }
    
    private void OnTriggerExit(Collider other)
    {
        if (!other.gameObject.CompareTag("Player"))
            return;
        _boxCollider.enabled = true;
        _sphereCollider.enabled = false;
    }

    private void TimeToExplosion(float t)
    {  _time++;
        if (!(_time * Time.deltaTime > t)) return;
        ExplosionType();
        Destroy(gameObject);
        GameManager.Get().activeBomb --;
        _time = 0;
    }

    private void ExplosionType()
    {
        Explosion(transform.position);

        Vector3 newPos = transform.position;
        newPos.z += 1;
        Restrictions(transform.forward,newPos);
        
        newPos = transform.position;
        newPos.z -=1;
        Restrictions(transform.forward*-1,newPos);
        
        newPos = transform.position;
        newPos.x +=1;
        Restrictions(transform.right,newPos);
        
        newPos = transform.position;
        newPos.x -=1;
        Restrictions(transform.right*-1,newPos);
        
    }
    private void Explosion(Vector3 position)
    {
        Instantiate(explosion, position, transform.rotation);
    }
    
    private void Restrictions(Vector3 rayDirection,Vector3 blastPosition)
    {
        RaycastHit hit;
        if (Physics.Raycast(transform.position, rayDirection, out hit, rayDistance, LayerMask.GetMask("Block")))
            return;
        Explosion(blastPosition);
    }

}

    