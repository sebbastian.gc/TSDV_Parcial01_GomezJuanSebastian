﻿using System;
using UnityEngine;
using Random = UnityEngine.Random;

public class Block : MonoBehaviour
{
     private Vector3 _position;
     private MeshRenderer _mesh;
     public GameObject door;
     public GameObject powerUp_ExtraLife;
     public GameObject powerUp_ExtraBomb;
     private short _score = 10;
     public delegate void Breakable();
     public static  Breakable OnBreak;
     private void Awake()
     {
         _mesh = GetComponentInChildren<MeshRenderer>();
         GameManager.OnInitGame += ActiveMesh;
     }
     private void OnDisable()
     {
         GameManager.OnInitGame -= ActiveMesh;
     }
     
     private void ActiveMesh()
     {
         _mesh.enabled = true;
     }

     private void OnCollisionEnter(Collision other)
     {  
        string collider = other.gameObject.tag;
        if (GameManager.Get().initGame) return;
        switch (collider)
        {   
            case"Block":
            case "Player":
            case "Breakable":
            case "Enemy":
                  //  LevelManager.OnReSpawn?.Invoke(_position); revisar 
                   ReSpawn();
                break;
        }
     }
     private void ReSpawn()
     {
         _position.x = Random.Range(0, 21);
         _position.z = Random.Range(0, 21);
         transform.position = _position;
     }
     public void DestroyBlock()
     {
         SpawnPowerUp();
         GameManager.Get().score += _score;
         if (GameManager.Get().activeDoor) return;
         OnBreak?.Invoke();
         if (!GameManager.Get().activeDoor) return;
         Instantiate(door, transform.position, transform.rotation);
     }

     private void SpawnPowerUp()
     {
         if (GameManager.Get().activeDoor) return;
         float num = Random.Range(0, 4);
         switch (num)
         {
             case 1:
                 Instantiate(powerUp_ExtraLife ,transform.position, transform.rotation);
                 break;
             case 3:
                 Instantiate(powerUp_ExtraBomb, transform.position, transform.rotation);
                 break;
         }
     }
     
}                           