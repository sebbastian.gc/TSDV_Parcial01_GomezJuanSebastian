﻿using UnityEngine;

public class PowerUpBomb : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            ExtraBomb();
            HudManager.OnBlockDestroy?.Invoke();
            Destroy(gameObject);
        }
    }
    private void ExtraBomb()
    {
        GameManager.Get().powerUpBomb = true;
    }
}


